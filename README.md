[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](http://standardjs.com/)

For beginners to learn the concepts in [Redux](https://github.com/reactjs/redux)

To run this example:


1. Clone:  git clone https://mikecanann@bitbucket.org/mikecanann/simplest-redux-example.git
2. From the repo folder run:  
   `yarn`
   `yarn run start`
3. open [http://localhost:8000/](http://localhost:8000/) in the browser

And also head over to http://redux.js.org/ for some great documentation.

There is also a simple version of [webpack](https://github.com/jackielii/simplest-redux-example/tree/webpack) and an [ES5](https://github.com/jackielii/simplest-redux-example/tree/es5) example.
